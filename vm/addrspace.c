/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *        The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include <proc.h>

/*
 * Note! If OPT_DUMBVM is set, as is the case until you start the VM
 * assignment, this file is not compiled or linked or in any way
 * used. The cheesy hack versions in dumbvm.c are used instead.
 *
 * UNSW: If you use ASST3 config as required, then this file forms
 * part of the VM subsystem.
 *
 */

struct addrspace *
as_create(void)
{
    struct addrspace *as;

    as = kmalloc(sizeof(struct addrspace));
    if (as == NULL) {
        return NULL;
    }
    as->regionHead = NULL;
    as->regionTail = NULL;
    return as;
}

int
as_copy(struct addrspace *old, struct addrspace **ret)
{
    struct addrspace *newas;

    if (old == NULL) {
        return EFAULT;
    }

    lock_acquire(as_lock);

    newas = as_create();
    if (newas == NULL) {
        return ENOMEM;
    }
    struct regionNode * currOldNode = old->regionHead;
    // Decide where to put copy of regions later
    while (currOldNode != NULL) {
        struct regionNode * tempNode = kmalloc(sizeof(struct regionNode));
        if (tempNode == NULL) {
            if (newas->regionHead != NULL) {
                freeASRegions(newas->regionHead);
                // Dont have to free hpt entries as they haven't been added yet
                kfree(newas);
                lock_release(as_lock);
                return ENOMEM;
            }
        }

        tempNode->userBase = currOldNode->userBase;
        tempNode->numPages = currOldNode->numPages;
        tempNode->readable = currOldNode->readable; 
        tempNode->writeable = currOldNode->writeable; 
        tempNode->executable = currOldNode->executable; 
        tempNode->regionType = currOldNode->regionType;
        tempNode->changeBackReadOnly = currOldNode->changeBackReadOnly;
        tempNode->next = NULL;

        // Set the head/tail appropriately
        if (newas->regionHead == NULL) {
            newas->regionHead = tempNode;
            newas->regionTail = tempNode;
        }
        else {
            newas->regionTail->next = tempNode;
            newas->regionTail = tempNode;
        }

        currOldNode = currOldNode->next;
    }

    // Copy data in the old as' frames to the new ones
    copyFrames(old, newas);
    
    *ret = newas;
    lock_release(as_lock);

    return 0;
}

void 
copyFrames(struct addrspace * oldas, struct addrspace * newas) {
    // Dont have to check argument memory if valid, ensured by as_copy
    struct regionNode * currOld = oldas->regionHead;
    struct regionNode * currNew = newas->regionHead;
    // Iterate through all the regions make copies of the pages
    while (currOld != NULL) {
        int numPages = currOld->numPages;
        vaddr_t oldPageAddr = currOld->userBase;
        vaddr_t newPageAddr = currNew->userBase;
        for (int i = 0; i < numPages; ++i) {
            // No need to offset address initially
            if (i != 0) {
                oldPageAddr += PAGE_SIZE;
                newPageAddr += PAGE_SIZE;
            }
            // Lookup the page associated with oldPageAddr
            bool writeable;
            paddr_t frameAddrFound = hptLookupEntry(oldas, oldPageAddr, &writeable);
            // Move to next address if lookup failed to find a page
            if (frameAddrFound == 0) {
                continue;
            }
            /* Otherwise create a new frame and copy the contents of
            * the old frame over */
            vaddr_t * newFrame = kmalloc(PAGE_SIZE);
            memcpy((void *)newFrame, (void *)PADDR_TO_KVADDR(frameAddrFound), PAGE_SIZE);
            // Add the entry to the hpt
            paddr_t paddrToAdd = KVADDR_TO_PADDR((vaddr_t)newFrame);
            writeable = currOld->writeable;
            bool result = hptAddEntry(newas, newPageAddr, paddrToAdd, writeable);
            if (result == false) {
                panic("Couldn't add the page to the hpt\n");
            }
        }
        // Don't forget to set the pointers to next to avoid an infinite loop
        currOld = currOld->next;
        currNew = currNew->next;
    }
}

void
as_destroy(struct addrspace *as)
{   
    if (as == NULL) {
        return;
    }

    lock_acquire(as_lock);
    // Free all the frames allocated to the regions
    hptRemoveASEntries(as);
    // Free the linked list of region nodes
    freeASRegions(as->regionHead);
    kfree(as);
    lock_release(as_lock);
    flushTLB();
}

void
freeASRegions(struct regionNode * curr) {
    if (curr == NULL) {
        return;
    }
    freeASRegions(curr->next);
    kfree(curr);
}

void
as_activate(void)
{
    struct addrspace *as;

    as = proc_getas();
    if (as == NULL) {
        /*
        * Kernel thread without an address space; leave the
        * prior address space in place.
        */
        return;
    }

    flushTLB();
}

void
as_deactivate(void)
{
    /*
    * Write this. For many designs it won't need to actually do
    * anything. See proc.c for an explanation of why it (might)
    * be needed.
    */

    flushTLB();
}

/*
 * Set up a segment at virtual address VADDR of size MEMSIZE. The
 * segment in memory extends from VADDR up to (but not including)
 * VADDR+MEMSIZE.
 *
 * The READABLE, WRITEABLE, and EXECUTABLE flags are set if read,
 * write, or execute permission should be set on the segment. At the
 * moment, these are ignored. When you write the VM system, you may
 * want to implement them.
 */
int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t memsize,
                 int readable, int writeable, int executable)
{
    if (as == NULL) {
        return EFAULT;
    }

    // Code taken from dumbvm to round up to the next page boundary
    memsize += vaddr & ~(vaddr_t)PAGE_FRAME;
    vaddr &= PAGE_FRAME;
    memsize = (memsize + PAGE_SIZE - 1) & PAGE_FRAME;

    int numPages = memsize / PAGE_SIZE;

    // Create the region
    struct regionNode * newRegion = kmalloc(sizeof(struct regionNode));  
    if (newRegion == NULL) {
        return ENOMEM;
    }

    newRegion->userBase = vaddr;
    newRegion->numPages = numPages;
    newRegion->regionType = AS_OTHER;
    newRegion->readable = false;
    newRegion->writeable = false;
    newRegion->executable = false;
    newRegion->changeBackReadOnly = false;
    // Set permissions
    if (readable)
        newRegion->readable = true;
    if (writeable)
        newRegion->writeable = true;
    if (executable)
        newRegion->executable = true;
    // For now assume all read/writable until implementation is almost done
    newRegion->next = NULL;
    // Add the region to the linked list of the addrspace

    lock_acquire(as_lock);

    if (as->regionHead == NULL) {
        as->regionHead = newRegion;
        as->regionTail = newRegion;
    } else {
        as->regionTail->next = newRegion;
        as->regionTail = newRegion;
    }

    lock_release(as_lock);

    return 0;
}

int
as_prepare_load(struct addrspace *as)
{
    struct regionNode * curr = as->regionHead;
    bool writable = true;
    while (curr != NULL) {
        if ((curr->readable == true) && (curr->writeable == false)) {
            curr->writeable = true;
            curr->changeBackReadOnly = true;
            hptChangePermissions(as, writable);
        }
        curr = curr->next;
    }
    flushTLB();
    return 0;
}

int
as_complete_load(struct addrspace *as)
{
    struct regionNode * curr = as->regionHead;
    bool writable = false;
    while (curr != NULL) {
        if (curr->changeBackReadOnly == true) {
            curr->writeable = false;
            curr->changeBackReadOnly = false;
            hptChangePermissions(as, writable);
        }
        curr = curr->next;
    }
    flushTLB();
    return 0;
}

int as_define_stack(struct addrspace *as, vaddr_t *stackptr) { 
    // Set the stack to start 16 pages before the end of useg
    vaddr_t temp = USERSTACK - (PROC_STACK_SIZE * PAGE_SIZE);
    as_define_region(as, temp, (size_t)(PROC_STACK_SIZE * PAGE_SIZE), 1, 1, 0);
    as->regionTail->regionType = AS_STACK; 
    *stackptr = temp; 
    return 0; 
}
