#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>

void initFrameTable() {
    if (frameTable == NULL) {
        int topOfRam = ram_getsize();
        // Place frame table just above the os161 code
        int topOfOS161 = ram_getfirstfree();

        // Calculate frames used by os161
        int os161FramesUsed = topOfOS161/PAGE_SIZE;
        if (topOfOS161 % PAGE_SIZE > 0) {
            os161FramesUsed += 1;
        }
        
        // Calculate frames used by the frame table
        int ftLen = topOfRam/PAGE_SIZE;
        int ftSize = ftLen * sizeof(int);
        int ftFramesUsed = (ftSize + sizeof(struct frameTableHead)) /PAGE_SIZE;
        // Since ftSize/PAGE_SIZE truncates, add a page if there happens to be a remainder
        if ((ftSize + sizeof(struct frameTableHead)) % PAGE_SIZE > 0) {
            ftFramesUsed += 1;
        }

        // Initialise the frame table
        int ftLoc = os161FramesUsed * PAGE_SIZE;
        frameTable = (struct frameTableHead *) PADDR_TO_KVADDR(ftLoc);
        frameTable->memLoc = ftLoc;
        frameTable->head = 0;
        frameTable->len = ftLen;
        frameTable->frames = (int *) ((char *) frameTable + sizeof(struct frameTableHead));
        
        // Calculate frames used by the hpt
        int hptLen = ftLen * 2;
        // Note: hpt entry can't be larger than 4 32 bit words
        int hptSize = hptLen * sizeof(struct hptEntry);
        int hptFramesUsed = (hptSize+sizeof(struct hashPagedTableHead))/PAGE_SIZE;
        // Since hptSize/PAGE_SIZE truncates, add a page if there happens to be a remainder
        if ((hptSize+sizeof(struct hashPagedTableHead)) % PAGE_SIZE > 0)
            hptFramesUsed += 1; 
        int hptLoc = (os161FramesUsed * PAGE_SIZE) + (ftFramesUsed * PAGE_SIZE) + sizeof(struct frameTableHead);
        // Initialise the hpt
        hpt = (struct hashPagedTableHead *) PADDR_TO_KVADDR(hptLoc);
        hpt->len = hptLen;
        hpt->entries = (struct hptEntry *) (PADDR_TO_KVADDR(hptLoc + sizeof(struct hashPagedTableHead)));
        for (int i = 0; i < hpt->len; ++i) {
            hpt->entries[i].pid = NULL;
            hpt->entries[i].pageNum = 0;
            hpt->entries[i].next = 0;
            hpt->entries[i].frameNum = 0;

        } 
        
        /* Set all frames that have been used by os161, the frame table and hpt to used
         * Then set all the free frames to point to the next free frame */
        int currFramesUsed = os161FramesUsed + ftFramesUsed + hptFramesUsed;
        for (int i = 0; i < ftLen-1; ++i) {
            //kprintf("curr loc: %p\n", (void *) &frameTable->frames[i]);
            if (i < currFramesUsed) {
                frameTable->frames[i] = USED; 
            } else {
                frameTable->frames[i] = i+1;
            }
        }
        // Last frame will not have a next free node
        frameTable->frames[ftLen-1] = NO_VALID_NEXT;
        // Set the head to the first free frame
        frameTable->head = currFramesUsed;
    }
    return;
}

static struct spinlock stealmem_lock = SPINLOCK_INITIALIZER;

/* Note that this function returns a VIRTUAL address, not a physical 
 * address
 * WARNING: this function gets called very early, before
 * vm_bootstrap().  You may wish to modify main.c to call your
 * frame table initialisation function, or check to see if the
 * frame table has been initialised and call ram_stealmem() otherwise.
 */

vaddr_t alloc_kpages(unsigned int npages)
{
    if (frameTable == NULL) {
        paddr_t addr;

        spinlock_acquire(&stealmem_lock);
        addr = ram_stealmem(npages);
        spinlock_release(&stealmem_lock);

        if (addr == 0)
            return 0;

        return PADDR_TO_KVADDR(addr);
    }

    // Don't have to support more then 1 page
    if (npages != 1)
        return (vaddr_t) NULL;

    if (frameTable->head == (int) NULL)
        return ENOMEM;

    // Move head to next free
    spinlock_acquire(&stealmem_lock);

    int* nodeToAssign = &(frameTable->frames[frameTable->head]);
    paddr_t loc = frameTable->head * PAGE_SIZE;
    frameTable->head = *nodeToAssign;
    *nodeToAssign = USED; 
    // Null the 4K page before returning it
    // Also remember to convert the address from physical to virtual to avoid a vm_fault
    int* curLoc = (int *)PADDR_TO_KVADDR(loc);
    int iterations = (int)(PAGE_SIZE / sizeof(int));
    
    // Zero out the memory
    for (int i = 0; i < iterations; ++i) {
        *curLoc = 0;
        ++curLoc;
    }
    //kprintf("currLoc after 0: %p\n", (void *) curLoc);
    //kprintf("Mem address returned: %p\n", (void *)PADDR_TO_KVADDR(loc));
    spinlock_release(&stealmem_lock);
    // Change this to return a virtual address instead of a frame number
    return PADDR_TO_KVADDR(loc);
}

void free_kpages(vaddr_t addr)
{
    /*
    Convert virtual address to frame number using page table or
    look up what frame number corresponds to that physical address
    Then make the frame point to the head and change the head to the new frame
    */
    spinlock_acquire(&stealmem_lock);
    if (frameTable != NULL) {        
        int frameNum = (KVADDR_TO_PADDR(addr) / PAGE_SIZE); 
        // Convert to frame number, -1 because table is 0 indexed. Tell HPT to
        // deallocate the memory regarding frames here add Frame back to free l$
        frameTable->frames[frameNum] = frameTable->head;
        frameTable->head = frameNum;
    } else {
        // otherwise 0 the page on free
        int* curLoc = (int *)KVADDR_TO_PADDR(addr);
        for (int i = 0; i < (int)(PAGE_SIZE / sizeof(int)); ++i) {
            *curLoc = 0;
            ++curLoc;
        }
    }
    spinlock_release(&stealmem_lock);
}

