#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>
#include <machine/tlb.h>
#include <spl.h>
#include <proc.h>
#include <synch.h>
#include <current.h>


bool hptAddEntry(struct addrspace * pid, vaddr_t vaddr, paddr_t paddr, bool writeable) {
    uint32_t i = hptHash(pid, vaddr);
    // If there is a collision use linear probing
    uint32_t prevEntry = i;
    bool loopFlag = false;

    lock_acquire(hpt_lock);
    // Iterate to find next free entry since there was a collision
    while (hpt->entries[i].pid != NULL && i != 0) {
        if ((int) i == hpt->len-1) {
            if (loopFlag == true) {
                lock_release(hpt_lock);
                // No valid entry to store in
                return false;
            } else {
                loopFlag = true;
            }
        }
        i = (i+1) % hpt->len;
    }
    // Add the entry
    /* NOTE: next is not set in the case that the previous head of the collision
       was removed */
    hpt->entries[i].pid = pid;
    uint32_t pageNum = vaddr & PAGE_FRAME;
    hpt->entries[i].pageNum = pageNum;
    // Remember to set the permissions
    uint32_t paddrToSet = paddr & PAGE_FRAME;
    paddrToSet |= TLBLO_VALID;
    if (writeable) {
        paddrToSet |= TLBLO_DIRTY;
    }
    hpt->entries[i].frameNum = paddrToSet;
    // Set the previous entry to point to the one just added if there was a collision
    if (prevEntry != 0 && prevEntry != i) {
        while (hpt->entries[prevEntry].next != 0) {
            prevEntry = hpt->entries[prevEntry].next;
        }
        hpt->entries[prevEntry].next = i;
    }

    lock_release(hpt_lock);
    // Entry added successfully
    return true;
}

paddr_t hptLookupEntry(struct addrspace * pid, vaddr_t vaddr, bool * writeable) {
    uint32_t i = hptHash(pid, vaddr);
    uint32_t pageNum = vaddr & PAGE_FRAME;
    // Search for the entry in the hpt
    while (pid != hpt->entries[i].pid || pageNum != hpt->entries[i].pageNum) {
        // Entry not found, return null address
        if (hpt->entries[i].next == 0) {
            return 0;
        } else {
            // Go to the next chained entry
            i = hpt->entries[i].next;
        }
    }
    // Get the write permission for the frame so we can set the entry in tlb later
    *writeable = hpt->entries[i].frameNum & TLBLO_DIRTY;
    // Add the offset bits to form the physical location in memory
    uint32_t offset = vaddr & OFFSET_MASK;
    paddr_t paddr = (hpt->entries[i].frameNum & PAGE_FRAME) | offset;
    return paddr;
}

void hptRemoveASEntries(struct addrspace * pid) {
    /* Process will be holding the lock from as_destroy so no need
     * to acquire the lock here */
    /* Iterate over entire hpt to delete/free entries of the 
     * addrspace to be destroyed */
    for (int i = 1; i < hpt->len; ++i) {
        if (hpt->entries[i].pid == pid) {
            // Remember to use bitwise AND to remove permission bits
            kfree((vaddr_t *) PADDR_TO_KVADDR(hpt->entries[i].frameNum & PAGE_FRAME));
            hpt->entries[i].pid = NULL;
            hpt->entries[i].pageNum = 0;
            hpt->entries[i].frameNum = 0;
            /* Note: don't delete the next value so that the entries in 
             * the chain can still be looked up*/
        }
    }
}

void hptChangePermissions(struct addrspace * as, bool writable) {
    // Finds the entries of an as and changes their writeable permission
    for (int i = 1; i < hpt->len; ++i) {
        if (hpt->entries[i].pid == as) {
            uint32_t frameNum = hpt->entries[i].frameNum;
            frameNum &= PAGE_FRAME;
            frameNum |= TLBLO_VALID;
            if (writable) {
                frameNum |= TLBLO_DIRTY;
            } 
            hpt->entries[i].frameNum = frameNum;
        }
    }
}

// Flushes the TLB, needs to be used on address space change
void flushTLB() {
    // disable interrupts before starting this, and enable after
    int spl = splhigh();
    for (int i = 0; i < NUM_TLB; ++i) {
        tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
    }
    splx(spl);
}


// Hash function
uint32_t hptHash(struct addrspace *as, vaddr_t faultaddr) {
    uint32_t index;
    index = (((uint32_t )as) ^ (faultaddr >> PAGE_BITS)) % hpt->len;
    return index;
}

void vm_bootstrap(void)
{
    /* Initialise VM sub-system.  You probably want to initialise your 
       frame table here as well.
    */
    initFrameTable();

    hpt_lock = lock_create("hptLock");
    if (hpt_lock == NULL) {
        panic("No memory for hpt_lock\n");
    }

    as_lock = lock_create("aslock");
    if (as_lock == NULL) {
        panic("No memory for as_lock\n");
    }
}

int
vm_fault(int faulttype, vaddr_t faultaddress)
{
    struct addrspace *as;
    faultaddress &= PAGE_FRAME;

    switch (faulttype) {
        case VM_FAULT_READONLY:
            return EFAULT;
        case VM_FAULT_READ:
        case VM_FAULT_WRITE:
        break;
        default:
        return EINVAL;
    }

    if (curproc == NULL) {
        return EFAULT;
    }

    as = proc_getas();
    if (as == NULL) {
        return EFAULT;
    }

    bool writeable;
    // Lookup the hash page table for an entry
    paddr_t frameNum = hptLookupEntry(as, faultaddress, &writeable);
    // If entry found, load the entry into the tlb
    if (frameNum != 0) {
        loadTLB(faultaddress, frameNum, writeable);
        return 0;
    }

    // Otherwise determine if the region is valid by checking our addrspace
    bool validRegion = false;
    struct regionNode * curr = as->regionHead;
    while (curr != NULL) {
        if (curr->regionType != AS_STACK) {
            vaddr_t tempUserBot = curr->userBase;
            vaddr_t tempUserTop = tempUserBot + (curr->numPages * PAGE_SIZE);
            if (faultaddress >= tempUserBot && faultaddress < tempUserTop) {
                validRegion = true;
                break;
            }
        } else {
            vaddr_t tempUserTop = curr->userBase;
            vaddr_t tempUserBot = tempUserTop - (curr->numPages * PAGE_SIZE);
            if (faultaddress >= tempUserBot && faultaddress < tempUserTop) {
                validRegion = true;
                break;
            }
        }
        curr = curr->next;
    }
    // Invalid region
    if (!validRegion) {
        return EFAULT;
    }
    // If no permissions on valid region, then we can't do anything with entry to tlb
    if (curr->readable == false && curr->writeable == false && curr->executable == false) {
        return EFAULT;
    }
    vaddr_t * newFrame = kmalloc(PAGE_SIZE);
    paddr_t paddr = KVADDR_TO_PADDR((vaddr_t) newFrame);
    // Set the address 
    // Since the region is valid, add a new entry to the hpt
    writeable = curr->writeable;
    bool entryAdded = hptAddEntry(as, faultaddress, paddr, writeable);
    // Something went wrong
    if (entryAdded != true) {
        return ENOMEM;
    }
    loadTLB(faultaddress, paddr, writeable);
    return 0;
}

void loadTLB(vaddr_t vaddr, paddr_t paddr, bool writeable) {
    int spl = splhigh();
    uint32_t ehi = vaddr;
    /* Set all to read write since current code has a tlb modify
     * exception when attempting to set the correct permissions*/
    uint32_t elo = paddr | TLBLO_VALID | TLBLO_DIRTY;
    if (writeable) {
        elo |= TLBLO_DIRTY;
    }
    tlb_random(ehi, elo);
    splx(spl);
}

/*
 *
 * SMP-specific functions.  Unused in our configuration.
 */

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
        (void)ts;
        panic("vm tried to do tlb shootdown?!\n");
}

